package tdd.training.bsk;

import java.util.ArrayList;

public class Game {
	ArrayList<Frame> frames = new ArrayList<Frame>();
	private Frame frame;
	private int calculateScore;
	private int firstBonus;
	private int secondBonus;
	private int totalBonus;

	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		// To be implemented
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		if(frames.size() < 10) {
			frames.add(frame);
		} else {
			throw new BowlingException("The number max of frames is 10");
		}
		
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		if(index >=0 && index <=9) {
			return frames.get(index);
		} else {
			throw new BowlingException("The index is not correct");
		} 
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		this.firstBonus = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		this.secondBonus = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return firstBonus;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return secondBonus;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		for (int i=0; i<10; i++) {
			frame = frames.get(i);			
			if(i == frames.size() -1 && !frame.isStrike()) {
				calculateScore += frame.getScore() + firstBonus;
			}else if (i == frames.size() -1 && frame.isStrike()) {
				calculateScore += frame.getScore() + firstBonus +secondBonus;
			}else if(frame.isStrike() && frames.get(i+1).isStrike()) {
				if(i == frames.size() -2) {
					totalBonus = frames.get(i+1).getFirstThrow() + firstBonus;
					frame.setBonus(totalBonus);
				} else {
					totalBonus = frames.get(i+1).getFirstThrow() + frames.get(i+2).getFirstThrow();
					frame.setBonus(totalBonus);
				}
				calculateScore += frame.getScore();
			}else if(frame.isStrike() && !frames.get(i+1).isStrike()) {
				frame.setBonus(frames.get(i+1).getScore());
				calculateScore += frame.getScore();
			} else if(frame.isSpare()){	
				frame.setBonus(frames.get(i+1).getFirstThrow());
				calculateScore += frame.getScore();
				
			} else if (frame.getScore() < 10) {
				calculateScore += frame.getScore();
			}
		}
		return calculateScore;
	}

}
