package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;


public class FrameTest {

	@Test
	public void testGetFirstThrow() throws Exception{
		Frame frame = new Frame(2,4);
		assertEquals(2,frame.getFirstThrow());
	}
	
	@Test
	public void testGetSecondThrow() throws Exception{
		Frame frame = new Frame(2,4);
		assertEquals(4,frame.getSecondThrow());
	}
	
	@Test (expected = BowlingException.class)
	public void testShouldBeTenPins() throws Exception{
		Frame frame = new Frame(6,5);
	}
	
	@Test
	public void testGetScore() throws Exception{
		Frame frame = new Frame(2,6);
		assertEquals(8,frame.getScore());
	}
	
	@Test
	public void testShouldBeGetTheBonusCorrect() throws Exception{
		Frame frame = new Frame(1,9);
		Frame frame2 = new Frame(3,6);
		frame.setBonus(frame2.getFirstThrow());
		assertEquals(3,frame.getBonus());
	}
	
	@Test
	public void testShouldBeSpare() throws Exception{
		Frame frame = new Frame(1,9);
		boolean isSpare = frame.isSpare();
		assertTrue(isSpare);
	}
	
	@Test
	public void testGetScoreShouldBeSpare() throws Exception{
		Frame frame = new Frame(1,9);
		Frame frame2 = new Frame(3,6);
		frame.setBonus(frame2.getFirstThrow());
		assertEquals(13,frame.getScore());
	}
	
	@Test
	public void testShouldBeStrike() throws Exception{
		Frame frame = new Frame(10,0);
		boolean isStrike = frame.isStrike();
		assertTrue(isStrike);
	}
	
	@Test
	public void testGetScoreShouldBeStrike() throws Exception{
		Frame frame = new Frame(10,0);
		Frame frame2 = new Frame(3,6);
		frame.setBonus(frame2.getScore());
		assertEquals(19,frame.getScore());
	}

	
}
