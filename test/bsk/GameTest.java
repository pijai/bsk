package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;
import tdd.training.bsk.Game;

public class GameTest {

	@Test (expected = BowlingException.class)
	public void testShouldBeTenFrames() throws Exception{
		Game game = new Game();
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		game.addFrame(new Frame(1,5));
	}
	
	@Test
	public void testFrameIndexShouldBeTheFrame() throws Exception{
		Game game = new Game();
		Frame frame1 = new Frame(1,5);
		game.addFrame(new Frame(1,5));
		Frame frame2 = new Frame(3,6);
		game.addFrame(frame2);
		assertEquals(frame2,game.getFrameAt(1));
	}
	
	@Test (expected = BowlingException.class)
	public void testFrameIndexIsCorrect() throws Exception{
		Game game = new Game();
		Frame frame1 = new Frame(1,5);
		game.addFrame(new Frame(1,5));
		game.getFrameAt(-1);
	}
	
	@Test
	public void testShouldBeEexactlyCalculatedScore() throws Exception{
		Game game = new Game();
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		assertEquals(81,game.calculateScore());
	}
	
	@Test
	public void testShouldBeEexactlyCalculatedScoreWithSpare() throws Exception{
		Game game = new Game();
		game.addFrame(new Frame(1,9));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		assertEquals(88,game.calculateScore());
	}
	
	@Test
	public void testShouldBeEexactlyCalculatedScoreWithStrike() throws Exception{
		Game game = new Game();
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		assertEquals(94,game.calculateScore());
	}
	
	@Test
	public void testShouldBeEexactlyCalculatedScoreWithStrikeAndSpare() throws Exception{
		Game game = new Game();
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(4,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		assertEquals(103,game.calculateScore());
	}
	
	@Test
	public void testShouldBeEexactlyCalculatedScoreWithMultipleStrike() throws Exception{
		Game game = new Game();
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		assertEquals(112,game.calculateScore());
	}
	
	@Test
	public void testShouldBeEexactlyCalculatedScoreWithMultipleSpare() throws Exception{
		Game game = new Game();
		game.addFrame(new Frame(8,2));
		game.addFrame(new Frame(5,5));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		assertEquals(98,game.calculateScore());
	}
	
	@Test
	public void testShouldBeEexactlyCalculatedScoreWithSpareAtLastFrame() throws Exception{
		Game game = new Game();
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,8));
		game.setFirstBonusThrow(7);
		assertEquals(90,game.calculateScore());
	}
	
	@Test
	public void testShouldBeEexactlyCalculatedScoreWithStrikeAtLastFrame() throws Exception{
		Game game = new Game();
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(10,0));
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);
		assertEquals(92,game.calculateScore());
	}
	
	@Test
	public void testShouldBeEexactlyCalculatedPerfectScore() throws Exception{
		Game game = new Game();
	
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.setFirstBonusThrow(10);
		game.setSecondBonusThrow(10);
		assertEquals(300,game.calculateScore());
	}
}
